package io.java.interfaces;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.java.dto.SquadDetails;

public interface SquadDetailsInterface extends MongoRepository<SquadDetails, String> {
	
	public List<SquadDetails> findByTribe(String tribe);
	
	
}
